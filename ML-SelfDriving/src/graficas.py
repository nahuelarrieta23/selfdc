
def animate(i):
   # Las siguientes dos lineas de codigo auto ajustan el eje
   # de las Y en funcion del contenido de la grafica. Me tomo
   # algo de tiempo encontrar estas funciones. Cuidenlo con su
   # alma y compartanlo!
   global ylim
   ax.relim()
   ax.autoscale_view()
   
   if ax.get_ylim() != ylim:
      # Esta parte del codigo lo que hace es monitorear los valores
      # del limite del eje Y para detectar cuando la grafica ha sido
      # reajustada. Esto para redibujar las etiquetas del eje Y a
      # medida que se reajusta. Si no, las etiquetas permanecen mientras
      # el eje se reajusta. Por lo que los valores no coinciden con lo
      # desplegado en el eje. Los invito a removerlo para que vean a
      # lo que me refiero.
      ylim = ax.get_ylim()
      fig.canvas.draw()
 
   for name, line in zip(signal.keys(), lines):
   # Si no hay datos nuevos, ni siquiera nos molestamos en intentar
   # graficar.
   if len(signal[name]) &gt; 0:
      _, ly = line.get_data()
      ly = np.append(ly, signal[name])
      _xdata = np.arange(ly.size)
      line.set_data(_xdata, ly)
 
      # La informacion ha sido graficada. Ya nos podemos deshacer
      # de ella.
      signal[name] = []
   else:
      print('Signal has no data')
return lines