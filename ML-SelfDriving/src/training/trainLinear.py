import torch
import torch.nn as nn
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import utils
import scipy
from scipy import signal
from torch.utils import data
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import MultiStepLR


lr = 1e-4
weight_decay = 1e-5
batch_size = 32
num_workers = 8
test_size = 0.8
shuffle = True

epochs = 80
start_epoch = 0
resume = False

#Hiper parametros
input_size = 1
output_size = 1
num_epochs = 100
learning_rate = 0.001

#Dataset: Usar pandas para obtener x_train
#como el steering angle y el y_train como aceleracion

def loadData(data_dir):
    """Load training data and train validation split"""
    # reads CSV file into a single dataframe variable
    data_df = pd.read_csv(os.path.join(data_dir, 'driving_log.csv'),
                          names=['center', 'left', 'right', 'steering', 'throttle', 'reverse', 'speed'])

    # smooth data signal with `savgol_filter`
    data_df["steering"] = signal.savgol_filter(data_df["steering"].values.tolist(), 51, 11)

    return data_df

myData = loadData('../img')

x_traingg = np.array(myData.iloc[:,3].tolist(), dtype = np.float32)
y_traingg = np.array(myData.iloc[:,4].tolist(), dtype = np.float32)

def normalizarLista(lista):
    

def listOfLists(lista):
    
    result = []
    for i in range(len(lista)):
        newElement = [lista[i]]
        result.append(newElement)
    
    return result

listOflistX = listOfLists(x_traingg)
listOflistY= listOfLists(y_traingg)

npArrayX = np.asarray(listOflistX, dtype = np.float32)
npArrayY = np.asarray(listOflistY, dtype = np.float32)



'''
#Lista de listas
x_train = np.array([[3.3], [4.4], [5.5], [6.71], [6.93], [4.168], 
 [9.779], [6.182], [7.59], [2.167], [7.042], 
 [10.791], [5.313], [7.997], [3.1]], dtype=np.float32)
y_train = np.array([[1.7], [2.76], [2.09], [2.19], [1.694], [1.573], 
 [3.366], [2.596], [2.53], [1.221], [2.827], 
 [3.465], [1.65], [2.904], [1.3]], dtype=np.float32)
'''
model = nn.Linear(input_size, output_size)

criterion = nn.MSELoss()
optimizer = torch.optim.SGD(model.parameters(), lr=learning_rate)

#Training
for epoch in range(num_epochs):
    inputs = torch.from_numpy(npArrayX) # Aqui irian las aceleraciones
    targets = torch.from_numpy(npArrayY) # Aqui irian los angles

    outputs = model(inputs)
    loss = criterion(outputs, targets)
    optimizer.zero_grad()
    loss.backward()
    optimizer.step()
 
    if (epoch+1) % 5 == 0:
        print ('Epoch [{}/{}], Loss: {:.4f}'.format(epoch+1, num_epochs, loss.item()))

predicted = model(torch.from_numpy(npArrayX)).detach().numpy()
plt.plot(npArrayX, npArrayY, 'ro', label='Original data')
plt.plot(npArrayX, predicted, label='Fitted line')
plt.legend()
plt.show()